## Movie app

Movie app is a React Native app, that contains examples of:

- React native stack-navigation between MoviesScreen and MovieScreen [Define stack in App](https://bitbucket.org/maaritv/movieapp/src/master/App.js) 
- Storing API-key to secure vault using React Native Encrypted Storage, which is 
recommended by React DEV-site. [API-key in ConfigurationScreen](https://bitbucket.org/maaritv/movieapp/src/master/screens/ConfigScreen.js)
    - https://reactnative.dev/docs/security#secure-storage
- Fetching data with with API-key in [API-key in MoviesComponent](https://bitbucket.org/maaritv/movieapp/src/master/components/movies/MoviesComponent.js)