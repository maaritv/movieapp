import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

/**
 * Parameters are predefined, when using Stack-navigator.
 * @param {Route and navigation} param0
 */

export const MovieScreen = ({route, navigation}) => {
  const movie = route.params.movie;
  console.log(JSON.stringify(movie));

  let img = require('../images/empty.jpg');

  if (movie.image) {
    img = {uri: movie.image.url};
  }

  return (
    <View>
      <Image style={styles.imageCard} source={img} />
      <Text style={styles.movieHeader}>{movie.title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  imageCard: {
    height: 350,
    width: 400,
    resizeMode: 'contain',
    marginTop: 20,
  },
  movieHeader: {
    fontFamily: 'Arial',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
