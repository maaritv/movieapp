import React, {useState} from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

import EncryptedStorage from 'react-native-encrypted-storage';

export async function getApiKey() {
  try {
    const apiKey = await EncryptedStorage.getItem('api_key');

    if (apiKey !== undefined) {
      return apiKey;
    }
    return null;
  } catch (error) {
    //console.log('error is ' + JSON.stringify(error));
    // There was an error on the native side
    return null;
  }
}

export const ConfigurationScreen = () => {
  const [text, setText] = useState('');
  return (
    <View>
      <Text>Add your API key to secure vault</Text>
      <TextInput onChangeText={setText} style={style.input} />
      <TouchableOpacity style={style.button} onPress={() => saveValue(text)}>
        <Text>Save value</Text>
      </TouchableOpacity>
    </View>
  );
};

async function saveValue(value) {
  try {
    await EncryptedStorage.setItem('api_key', value);
    console.log('stored now!');

    // Congrats! You've just stored your first value!
  } catch (error) {
    console.log('error when storing ' + JSON.stringify(error));
  }
}

const style = StyleSheet.create({
  button: {
    width: 100,
    height: 30,
    backgroundColor: '#00FF00',
    textAlign: 'center',
    alignSelf: 'center',
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});
