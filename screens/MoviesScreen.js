import React from 'react';
import {useState, useEffect} from 'react';
import {View, Text, Style, LogBox} from 'react-native';
import {MoviesComponent} from '../components/movies/MoviesComponent';
import {getApiKey} from './ConfigScreen';

/**
 * This is to ignore warning about delivering functions, class instances etc
 * (params.navigation.replace function in this case)
 * along with the navigation to child screen. It is ok, to ignore, if
 * there is no state in the destination screen (MovieScreen). Now there is not.
 */

function MovieException(message) {
  const error = new Error(message);
  return error;
}

MovieException.prototype = Object.create(Error.prototype);

export const MoviesScreen = ({route, navigation}) => {
  const [searchKey, setSearchKey] = useState('rose');
  const [apiKey, setApiKey] = useState(null);

  LogBox.ignoreLogs([
    'Non-serializable values were found in the navigation state',
  ]);

  useEffect(() => {
    let mounted = true;

    //This enables using of async functions without the callback,
    //use effect has to be synchronic.
    
    async function getKey() {
      return await getApiKey();
    }
     
    getApiKey().then(key => setApiKey(key));
    if (route.params.searchKey) {
      setSearchKey(route.params.searchKey);
    }

    console.log(searchKey);
    return () => (mounted = false);
  }, []);

  if (apiKey == null) {
    return (
      <View>
        <Text>Please, configure an API-key!</Text>
      </View>
    );
  }

  return (
    <View>
      <MoviesComponent
        apiKey={apiKey}
        searchKey={searchKey}
        navigation={navigation}
      />
    </View>
  );
};
