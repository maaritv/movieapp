import React from 'react';
import {useState, useEffect} from 'react';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  LogBox,
} from 'react-native';

async function getMovies(callback, searchKey, apiKey) {
  let interestedIn = 'tvSeries';
  if (apiKey == null) {
    callback([]);
  }
  //Do not configure sensitive information to source code like this. It is uploaded to device
  //in clear text and not even compiled.
  //apiKey='sfhjksfjksfj';
  const url = `https://imdb8.p.rapidapi.com/title/find?q=${searchKey}`;
  fetch(url, {
    method: 'GET',
    headers: {
      'x-rapidapi-host': 'imdb8.p.rapidapi.com',
      'x-rapidapi-key': apiKey,
    },
  })
    .then(response => response.json())
    .then(json => {
      const interestingMovies = collectTypesOf(interestedIn, json.results);
      //console.log(JSON.stringify(interestingMovies));
      callback(interestingMovies);
    })
    .catch(error => {
      callback(null);
    });
}

function collectTypesOf(interestedIn, data) {
  let interestingMovies = [];
  data.forEach(interest => {
    if (interest.titleType === interestedIn) {
      interestingMovies.push(interest);
    }
  });
  return interestingMovies;
}

const Item = ({movie, navigation}) => {
  console.log(movie.title);
  return (
    <TouchableOpacity
      style={styles.button}
      onPress={() => showMovie(movie, navigation)}>
      <View style={styles.item}>
        <Text style={styles.itemTitle}>
          {movie.title} {movie.year}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

/**
 * Function navigates the user to one movie screen 
 * with the selected movie object.
 * @param {} movie 
 * @param {*} navigation 
 */

function showMovie(movie, navigation) {
  console.log(`Show movie ${movie.year}`);
  navigation.push('Movie', {
    movie: movie,
  });
}

export const MoviesComponent = ({navigation, apiKey, searchKey}) => {
  const [movies, setMovies] = useState(null);

  LogBox.ignoreLogs([
    'Non-serializable values were found in the navigation state',
  ]);

  useEffect(() => {
    let mounted = true;

    console.log(searchKey);
    if (apiKey !== null && searchKey !== null) {
      getMovies(setMovies, searchKey, apiKey);
      /* let m = [
                                 {
                                   id: 1,
                                   title: 'Eka',
                                   image: {
                                     url: 'https://m.media-amazon.com/images/M/MV5BZTY4NDBiNTMtNGY0ZC00NzUxLWJiMTctN2MwZjM3ZjEwNDM2XkEyXkFqcGdeQXVyMjc2MzA3ODM@._V1_.jpg',
                                   }
                                   year: 2003,
                                 },
                                 {
                                   id: 2,
                                   title: 'Toka',
                                   year: 2000,
                                   image: {
                                     url: 'https://m.media-amazon.com/images/M/MV5BZTY4NDBiNTMtNGY0ZC00NzUxLWJiMTctN2MwZjM3ZjEwNDM2XkEyXkFqcGdeQXVyMjc2MzA3ODM@._V1_.jpg',
                                   },
                                 },
                               ];
                               setMovies(m);
                               */
    }
    return () => (mounted = false);
  }, []);

  //console.log('movies are ' + JSON.stringify(navigation));

  const renderItem = ({item}) => <Item movie={item} navigation={navigation} />;

  if (movies == null) {
    return (
      <View>
        <Text>Loading...</Text>
      </View>
    );
  }

  return (
    <FlatList
      data={movies}
      renderItem={renderItem}
      keyExtractor={item => item.id}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    height: 70,
    backgroundColor: '#88B1C4',
    padding: 20,
    borderColor: '#88B1C4',
    marginVertical: 3,
    marginHorizontal: 6,
  },
  itemTitle: {
    //do not use reserved words alone like "title", it can crash the app.
    fontSize: 24,
    fontWeight: '600',
  },
  button: {
    height: 90,
    backgroundColor: '#FFFFFF',
    fontSize: 24,
    fontWeight: '600',
  },
  buttonText: {
    fontSize: 24,
    fontWeight: '600',
    textAlign: 'center',
    textDecorationColor: '#FFFFFF',
  },
});
