/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import {Node, useState, useEffect} from 'react';
import {
  TouchableOpacity, //Do not import this from gesture-handler.
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {MovieScreen} from './screens/MovieScreen';
import {MoviesScreen} from './screens/MoviesScreen';
import {ConfigurationScreen, getApiKey} from './screens/ConfigScreen';

/**
 * Do not wait until response is got. When it is ready, callback-function will
 * update the component state.
 *
 * @param {*} callback
 */

const App: () => Node = () => {
  const Stack = createNativeStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={SearchScreen}
          options={{title: 'Search'}}
        />
        <Stack.Screen
          name="Config"
          component={ConfigurationScreen}
          options={{title: 'Config'}}
        />
        <Stack.Screen
          name="Movies"
          component={MoviesScreen}
          options={{title: 'TV Series'}}
        />
        <Stack.Screen
          name="Movie"
          component={MovieScreen}
          options={{title: 'TV Series'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const SearchScreen = ({navigation}) => {
  const [str, setStr] = useState('');

  return (
    <View>
      <Text>Search for the TV-series</Text>

      <TextInput style={styles.input} onChangeText={setStr} placeholder="" />
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.push('Movies', {searchKey: str})}>
        <Text style={styles.buttonStyle}>Search</Text>
      </TouchableOpacity>
      <Text style={styles.configLink} onPress={() => navigation.push('Config')}>
        {' '}
        Configure!
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  configLink: {
    color: '#000000',
    fontSize: 20,
  },
  button: {
    height: 90,
    width: '50%',
    alignSelf: 'center',
    backgroundColor: '#A891BE', //remember to add a backgroundcolor, button can not be seen otherwise
    fontSize: 24,
    fontWeight: '600',
    alignItems: 'center',
    paddingTop: 30,
  },
  buttonStyle: {
    fontSize: 20,
    color: '#FFFFFF',
  },
});

export default App;
